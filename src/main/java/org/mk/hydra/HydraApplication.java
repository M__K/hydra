package org.mk.hydra;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @description:
 * @author: SJP
 * @date: 2021/7/15
 */

@Slf4j
@EnableScheduling
@SpringBootApplication
public class HydraApplication {

    private static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(HydraApplication.class);
        application.addListeners(new ApplicationPidFileWriter("./hydra.pid"));
        context = application.run();
        log.info("Server starting ......");
        systemInfo();
        log.info("Server start finished");
    }

    public static <T> T getBean(Class<T> clazz){
        return context.getBean(clazz);
    }

    private static void systemInfo() {
        int mb = 1024 * 1024;
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
        log.info("##### Heap utilization statistics [MB] #####");
        //Print used memory
        log.info("System core count: " + runtime.availableProcessors());
        log.info("Used Memory: "
                + (runtime.totalMemory() - runtime.freeMemory()) / mb);
        //Print free memory
        log.info("Free Memory: "
                + runtime.freeMemory() / mb);
        //Print total available memory
        log.info("Total Memory: " + runtime.totalMemory() / mb);
        //Print Maximum available memory
        log.info("Max Memory: " + runtime.maxMemory() / mb);
    }


}
